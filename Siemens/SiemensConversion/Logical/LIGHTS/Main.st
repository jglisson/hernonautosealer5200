
PROGRAM _INIT
	//"Blink" function blocks for stack lights, button colors.
	Blink_0.Enable		:= TRUE;
	Blink_0.Period 		:= 1;
	Blink_0.DutyCycle 	:= 50;
	Blink_0();
	 
END_PROGRAM

PROGRAM _CYCLIC
	
//	Status Light
//	b. Solid Red is ESTOP OR Error Stop State
//	c. Flashing Red is Bypass Key switch active WHILE running
//	d. Solid Green is Normal Operation
//	e. Flashing Yellow is Starting state
//	f. Solid Yellow is Stop State
//	g. Flashing Blue is Low Sealant level WHILE running
	
	//CASE statement mirrors MAIN task but only sets stack lights
	//NOTE: This task should ONLY be READING "MachineState", shouldn't be altering it!
	CASE MachineState OF
		mINIT:
			oGreenStatusLight	:=	FALSE;
			oYellowStatusLight	:=	Blink_0.Out;
			oBlueStatusLight	:=	FALSE;
			oRedStatusLight		:=	FALSE;
		
		mWAIT:
			oGreenStatusLight	:=	(NOT iMuteSwitchStatus) OR (iMuteSwitchStatus AND Blink_0.Out);
			oYellowStatusLight	:=	FALSE;
			oBlueStatusLight	:=	FALSE;
			oRedStatusLight		:=	FALSE;
		
		mRUNNING:
			oGreenStatusLight	:=	(NOT iMuteSwitchStatus) OR (iMuteSwitchStatus AND Blink_0.Out);
			oYellowStatusLight	:=	FALSE;
			oBlueStatusLight	:=	Blink_0.Out AND iLowReservoirLevel;
			oRedStatusLight		:=	FALSE;
		
		mMANUAL:
			oGreenStatusLight	:=	FALSE;
			oYellowStatusLight	:=	FALSE;
			oBlueStatusLight	:=	FALSE;
			oRedStatusLight		:=	FALSE;
		
		mSTOPPED:
			oGreenStatusLight	:=	FALSE;
			oYellowStatusLight	:=	TRUE;
			oBlueStatusLight	:=	FALSE;
			oRedStatusLight		:=	FALSE;
		
		mEMERGENCY_STOP:
			oGreenStatusLight	:=	FALSE;
			oYellowStatusLight	:=	FALSE;
			oBlueStatusLight	:=	FALSE;
			oRedStatusLight		:=	TRUE;
		
		mWAIT_RESET:
			oGreenStatusLight	:=	FALSE;
			oYellowStatusLight	:=	FALSE;
			oBlueStatusLight	:=	FALSE;
			oRedStatusLight		:=	FALSE;
		
	END_CASE;
	
	Blink_0.Enable := TRUE;
	Blink_0();
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

