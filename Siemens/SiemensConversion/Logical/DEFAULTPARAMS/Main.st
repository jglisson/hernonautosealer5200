
PROGRAM _INIT
	cfgDispense_Offset[PROJECTILE] := 3;
	cfgDispense_Offset[PRIMER] := 5;
	
	cfgEject_Verify_Offset := 100;
	
	cfgInspect_Offset[PROJECTILE] := 16;
	cfgInspect_Offset[PRIMER] := 20;
	
	cfgMotor_Direction_REVERSE := FALSE;
	
	cfgReject_Offset := 85;
	
	recAccept_Enable := TRUE;
	recAccept_Fractional_Offset_Pct := 50;
	recAccept_Pulse_Time_ms := 35;
	
	recCure_Enable[0] := TRUE;
	recCure_Enable[1] := TRUE;
	recCure_Enable[2] := TRUE;

	recDispense_Enable[PROJECTILE] := TRUE;
	recDispense_Enable[PRIMER] := TRUE;
	recDispense_Pulse_Time_ms[PROJECTILE] := 2;
	recDispense_Pulse_Time_ms[PRIMER] := 2;
	recDisp_Fractional_Offset_Pct[PROJECTILE] := 50;
	recDisp_Fractional_Offset_Pct[PRIMER] := 50;
	
	recEjctVfy_Fractional_Offset_Pct := 50;
	recEject_Verify_Enable := TRUE;
	
	recEncoder_Backwards_Count_Fault := 20;

	recInspect_Enable[PROJECTILE] := TRUE;
	recInspect_Enable[PRIMER] := TRUE;
	recInspect_Fractional_Offset_Pct[PROJECTILE] := 50;
	recInspect_Fractional_Offset_Pct[PRIMER] := 50;
	recInspect_Num_Sqnt_Fault_ct[PROJECTILE] := 10;
	recInspect_Num_Sqnt_Fault_ct[PRIMER] := 10;
	recInspect_Num_total_Fault_ct[PROJECTILE] := 50;
	recInspect_Num_total_Fault_ct[PRIMER] := 50;
	recInspect_Program_Select			:= 	1;
	recInspect_Trigger_Time_ms[PROJECTILE] := 2;
	recInspect_Trigger_Time_ms[PRIMER] := 2;
	
	recMotor_Speed_Setpoint := 240;
	
	recReject_Enable := TRUE;
	recReject_Fractional_Offset_Pct := 50;
	recReject_Pulse_Time_ms := 35;
END_PROGRAM

PROGRAM _CYCLIC
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

