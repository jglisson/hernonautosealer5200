
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
		//
	IF VBlockIdxAtPiP <> _VBlockIdxAtPiP THEN													//Trigger on the scan after the VBlock Idx at the PiP sensor changes
		VBlock_At_Eject_Verify := VBlocks[VBlock_Idx_At_Eject_Verify];							//Check-out current VBlock at eject verify sensor
		
		IF iEjectVerify THEN																	//When Eject Verify sensor goes HIGH...
			IF VBlock_At_Eject_Verify.Accepted OR VBlock_At_Eject_Verify.Rejected THEN			//...And the block was supposed to have been kicked at accept/reject already...
				Fault_Condition := TRUE;														//...Raise fault condition.
			END_IF
		END_IF
		
		VBlocks[VBlock_Idx_At_Eject_Verify] := VBlock_At_Eject_Verify;							//Check VBlock @ eject verify back in
	END_IF
	
END_PROGRAM
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

