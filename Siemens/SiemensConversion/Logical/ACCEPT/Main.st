
PROGRAM _INIT
	TOF_Accept_Kicker.PT := T#250ms;							//Set default time for accept kicker
	 
END_PROGRAM

PROGRAM _CYCLIC
	osFlag_Kick_Accept := FALSE;								//Reset one-shot flag triggering the accept kicker
	
	VBlock_At_Accept := VBlocks[VBlock_Idx_At_Accept];			//"Check Out" the current VBlock
	
	IF (VBlockFractPosAtPiP_pct > cfgAccept_Offset
		AND _VBlockFractPosAtPiP_pct < cfgAccept_Offset			//Trigger on scan with the transition over the Accept fractional offset
		AND VBlock_At_Accept.PartPresent						//Part must be present in block
		AND VBlock_At_Accept.Inspection_Pass					//Part passed inspection
		AND NOT VBlock_At_Accept.Accepted)						//Current block hasn't already been kicked at accept, in case the line moved backwards then forwards
		OR Command_Manual_Accept_Kick 	THEN					//Also respect manual trigger
		osFlag_Kick_Accept := TRUE;
		
	END_IF
	
	TOF_Accept_Kicker.IN := osFlag_Kick_Accept;					//TOF off-timer keeps the actuator extended for long enough to actuate
	TOF_Accept_Kicker();
	oAcceptActuator := TOF_Accept_Kicker.Q;
	
	VBlocks[VBlock_Idx_At_Accept] := VBlock_At_Accept;			//"Check In" VBlock at accept unit
	Command_Manual_Accept_Kick := FALSE;						//Reset manual bit
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

