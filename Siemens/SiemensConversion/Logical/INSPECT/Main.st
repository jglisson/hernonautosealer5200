
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
		
	//General algorithm used here: Check out VBlock into "VBlock_At_Inspect[x]", check back in once you've done the operations/conditionals involved
	//Ideally this would be made as 2x FBK instances w/ VAR_INTERNAL, but for cross-compatibility's sake I'm keeping to basic flow control only.
	FOR ii := 0 TO MAX_IDX_INSPECT_CAMS BY 1 DO    												//Iterate once through for each inspection camera
		VBlock_At_Inspect[ii] := VBlocks[VBlock_Idx_At_Inspect[ii]]; 							//"Check Out" the current VBlock at the INSPECTION unit
		
		//On cycle where fractional position transitions across the dispense fractional offset (both 0-100%), if there is a part present and it hasn't already been inspected by this camera, set inspection trigger flag
		IF (VBlockFractPosAtPiP_pct > recInspect_Fractional_Offset_Pct[ii] 						//Current fractional position greater than programed inspection offset percentage
			AND _VBlockFractPosAtPiP_pct < recInspect_Fractional_Offset_Pct[ii] 					//Last scan's fractional position less than programed inspection offset percentage
			AND VBlock_At_Inspect[ii].PartPresent 												//VBlock currently at this camera has a part present
			AND NOT VBlock_At_Inspect[ii].Inspected[ii])										//Vblock currently at this camera has not been inspected by this cam yet
			OR Command_Manual_Inspect_Trigger[ii] THEN											//Also fire on manual command, ignoring the rest of the conditions
			osFlagInspectNow[ii] := TRUE;														//one-shot flag to command Inspection cycle to start
			Waiting_Inspection_Busy_Signal[ii] := TRUE;											//Flag inspection process is active
			VBlock_At_Inspect[ii].Inspected[ii] := TRUE; 										//Mark checked-out vblock that it has already been inspected on by this cam (thus the 2 iterators).
		END_IF
		
		//Below could also be done in ladder logic more efficiently if Siemens supports it->
		//Use the inspection trigger flag to activate the TOF timers
		TOF_Inspection_Trigger[ii].IN := osFlagInspectNow[ii];
		TOF_Inspection_Trigger[ii]();
		oTriggerInspect[ii] := TOF_Inspection_Trigger[ii].Q;									//IO Output "Inspection Trigger" held HIGH for duration of timer
		osFlagInspectNow[ii] := FALSE;
		TOF_Inspection_Wait_Busy[ii].IN := oTriggerInspect[ii];									//OFF timer for max amount of time to wait for "Inspection Busy" input begins at the falling edge of the trigger input
		TOF_Inspection_Wait_Busy[ii]();
		
		IF Waiting_Inspection_Busy_Signal[ii] AND iInspectionBusy[ii]  THEN						//Unlatch signal for waiting for "Inspection Busy" signal (b.c. sometimes it takes a few cycles after the "Inspection Trigger" to register)
			Waiting_Inspection_Busy_Signal[ii] := FALSE;
			Waiting_Inspection_Finish[ii] := TRUE;
		ELSIF Waiting_Inspection_Busy_Signal[ii] AND NOT TOF_Inspection_Wait_Busy[ii].Q THEN
			Waiting_Inspection_Busy_Signal[ii] := FALSE;
			Fault_Condition := TRUE;
		END_IF
		
		IF Waiting_Inspection_Finish[ii] AND NOT iInspectionBusy[ii] THEN						//On falling edge of "Inspection Busy"...
			VBlock_At_Inspect[ii].Inspection_Pass := iInspectionGood[ii];						//...Latch value of "Inspection Good" signal into the VBlock currently being inspected..
			Waiting_Inspection_Finish[ii] := FALSE;												//...Reset flag indicating task is waiting for inspection to complete
		END_IF
		
		VBlocks[VBlock_Idx_At_Inspect[ii]] := VBlock_At_Inspect[ii];							//"Check In" VBlock at inspect unit
		_VBlock_At_Inspect[ii] := VBlock_At_Inspect[ii];										//Latch current cycle's values to use next cycle;
		_VBlock_Idx_At_Inspect[ii] := VBlock_Idx_At_Inspect[ii];								//Latch current index of vblock at inspect to use next cycle;
		Command_Manual_Inspect_Trigger[ii] := FALSE;											//RESET manual inspection trigger bit
		
		
	END_FOR
	
	//Map recipe selection BOOL outputs to the selected recipe integer bits
	oBCDRecipeSelect_bit0 := recInspect_Program_Select.0;
	oBCDRecipeSelect_bit1 := recInspect_Program_Select.1;
	oBCDRecipeSelect_bit2 := recInspect_Program_Select.2;
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

