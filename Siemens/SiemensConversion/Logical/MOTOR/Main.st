
//TODO: Fault when chain moved backwards and machine NOT in RUN?
//TODO: What to do with gear tooth sensor?
//NOTE: in the original AS5200, we used a completely different method to calculate line position...
//...Instead of using the absolute encoder position, we used the high speed input w/ time stamp and then extrapolated based on time.
//...We did this because we noticed that the encoder was slightly non-linear and thus we were triggering inspection and dispense at the wrong time, but only on parts of the dial
//TODO: Track if line has moved X encoder ticks backwards, set FAULT flag

PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	//Math for rollover of UINT encoder value into DINT, accounting for positive and negative movement detection
	IF (iEncoderValue - _iEncoderValue) > iEncoderValue AND iEncoderValue < 32767 AND _iEncoderValue > 32767 THEN  //Negative rollover, 1->65535
		AbsoluteMultiturnPosition := AbsoluteMultiturnPosition + (iEncoderValue - _iEncoderValue) + 65536;
	ELSIF (_iEncoderValue - iEncoderValue) > _iEncoderValue AND iEncoderValue > 32767 AND _iEncoderValue < 32767 THEN  //Positive rollover, 65535 -> 1
		AbsoluteMultiturnPosition := AbsoluteMultiturnPosition + (iEncoderValue - _iEncoderValue) - 65536;
	ELSE
		AbsoluteMultiturnPosition := AbsoluteMultiturnPosition + (iEncoderValue - _iEncoderValue); //No rollover, just add/subtract
	END_IF
	
	//Calculate backwards encoder count and set fault condition
	IF AbsoluteMultiturnPosition > _AbsoluteMultiturnPosition THEN
		Encoder_Cycle_Count_diff := 0;
	ELSE
		Encoder_Cycle_Count_diff := Encoder_Cycle_Count_diff + (AbsoluteMultiturnPosition - _AbsoluteMultiturnPosition);
	END_IF
	
	IF (Encoder_Cycle_Count_diff * -1) > recEncoder_Backwards_Count_Fault THEN
		Fault_Condition := TRUE;
	END_IF
	
	//Calculate current indices of VBlock at Part In Place sensor
	EncoderTicksPerVBlock	:=	65536 / NUM_TEETH_PER_SPROCKET;
	VBlockIdxAtPiP	:=	(AbsoluteMultiturnPosition / EncoderTicksPerVBlock) MOD (MAX_IDX_PART_ARRAY + 1);
	VBlockFractPosAtPiP	:= AbsoluteMultiturnPosition MOD EncoderTicksPerVBlock;
	VBlockFractPosAtPiP_pct := 100 * (UDINT_TO_REAL(VBlockFractPosAtPiP) / UDINT_TO_REAL(EncoderTicksPerVBlock));
	
	//Set global tags indicating which VBlock index is at which station.
	VBlock_Idx_At_Dispense[0] 	:=		(VBlockIdxAtPiP + ((MAX_IDX_PART_ARRAY + 1) - cfgDispense_Offset[0])) 		MOD (MAX_IDX_PART_ARRAY + 1);
	VBlock_Idx_At_Dispense[1] 	:=		(VBlockIdxAtPiP + ((MAX_IDX_PART_ARRAY + 1) - cfgDispense_Offset[1])) 		MOD (MAX_IDX_PART_ARRAY + 1);
	VBlock_Idx_At_Inspect[0] 	:=		(VBlockIdxAtPiP + ((MAX_IDX_PART_ARRAY + 1) - cfgInspect_Offset[0])) 		MOD (MAX_IDX_PART_ARRAY + 1);
	VBlock_Idx_At_Inspect[1] 	:=		(VBlockIdxAtPiP + ((MAX_IDX_PART_ARRAY + 1) - cfgInspect_Offset[1])) 		MOD (MAX_IDX_PART_ARRAY + 1);
	VBlock_Idx_At_Accept		:=		(VBlockIdxAtPiP + ((MAX_IDX_PART_ARRAY + 1) - cfgAccept_Offset))			MOD (MAX_IDX_PART_ARRAY + 1);
	VBlock_Idx_At_Reject		:=		(VBlockIdxAtPiP + ((MAX_IDX_PART_ARRAY + 1) - cfgReject_Offset)) 			MOD (MAX_IDX_PART_ARRAY + 1);
	VBlock_Idx_At_Eject_Verify	:=		(VBlockIdxAtPiP + ((MAX_IDX_PART_ARRAY + 1) - cfgEject_Verify_Offset)) 	MOD (MAX_IDX_PART_ARRAY + 1);
	
	oPWMPeriod := 2000;			//PWM Period = IO scan time, [uS]
	oPulseTrainMotorSpeed := 	recMotor_Speed_Setpoint;
	
	_iEncoderValue := iEncoderValue;
	_AbsoluteMultiturnPosition := AbsoluteMultiturnPosition;
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

