
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
	osFlag_Kick_Reject := FALSE;								//Reset one-shot flag triggering the Reject kicker
	
	VBlock_At_Reject := VBlocks[VBlock_Idx_At_Reject];			//"Check Out" the current VBlock
	
	IF VBlockFractPosAtPiP_pct > cfgReject_Offset
		AND _VBlockFractPosAtPiP_pct < cfgReject_Offset			//Trigger on scan with the transition over the Reject fractional offset
		AND VBlock_At_Reject.PartPresent						//Part must be present in block
		AND VBlock_At_Reject.Inspection_Pass					//Part passed inspection
		AND NOT VBlock_At_Reject.Rejected						//Current block hasn't already been kicked at Reject, in case the line moved backwards then forwards
		OR Command_Manual_Reject_Kick 	THEN					//Also respect manual trigger
		
		osFlag_Kick_Reject := TRUE;
		
	END_IF
	
	TOF_Reject_Kicker.IN := osFlag_Kick_Reject;					//TOF off-timer keeps the actuator extended for long enough to actuate
	TOF_Reject_Kicker();
	oRejectActuator := TOF_Reject_Kicker.Q;
	
	VBlocks[VBlock_Idx_At_Reject] := VBlock_At_Reject;			//"Check In" VBlock at Reject unit
	Command_Manual_Reject_Kick := FALSE;						//Reset manual bit
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

