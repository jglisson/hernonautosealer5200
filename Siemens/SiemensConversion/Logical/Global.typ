
TYPE
	AS5200_Statistics_typ : 	STRUCT 
		Total_Inspection_Pass : UDINT;
		Total_Inspection_Fail : UDINT;
		total_runtime : UDINT;
		runtime_h : UDINT;
		runtime_m : UDINT;
		runtime_s : UDINT;
		Total_Parts : UDINT;
	END_STRUCT;
	VBlock_typ : 	STRUCT 
		PartPresent : BOOL;
		Dispensed : ARRAY[0..MAX_IDX_DISPENSE_VALVES]OF BOOL;
		Inspected : ARRAY[0..MAX_IDX_INSPECT_CAMS]OF BOOL;
		Inspection_Pass : BOOL;
		Rejected : BOOL;
		Accepted : BOOL;
	END_STRUCT;
	AS5200_States_Enum : 
		(
		mINIT := 0,
		mWAIT := 3,
		mRUNNING := 5,
		mMANUAL := 6,
		mSTOPPED := 7,
		mEMERGENCY_STOP := 8,
		mWAIT_RESET := 9
		);
END_TYPE
