
PROGRAM _INIT
	MachineState := mINIT;
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	IF EStop_Condition THEN													//If process voltage cut due to E-stop, door, etc. then immediately send machine to EMERGENCY_STOP state
		MachineState := mEMERGENCY_STOP;
	ELSIF Fault_Condition THEN												//Flag "Fault_Condition" is SET whenever there is a condition that should stop the machine.
		MachineState := mSTOPPED;
	END_IF
	
	Fault_Condition := FALSE;												//Reset Fault_Condition flag
	
	CASE MachineState OF													//MASTER STATE MACHINE.
		mINIT:																//Initial state.  Return to this state after Fault/Estop to make sure machine is ready to run
			IF Hardware_Online THEN
				MachineState := mWAIT;
			END_IF
		
		mWAIT:																//Machine is waiting for command.  Can be pushed into STOP or MANUAL mode.
			IF CommandStop THEN
				MachineState := mSTOPPED;
			ELSIF CommandManual THEN
				MachineState := mMANUAL;
			ELSIF CommandStart AND Machine_Ready_To_Run THEN
				MachineState := mRUNNING;
			END_IF
		
		mRUNNING:															//Machine is in RUN state.  
			IF CommandStop THEN
				MachineState := mSTOPPED;
			END_IF
		
		mMANUAL:															//Machine in MANUAL mode-> lets you freely poke IO or individually operate each station
			IF CommandStart OR CommandStop THEN
				MachineState := mSTOPPED;
			END_IF
		
		mSTOPPED:															//Machine has encountered an application fault or was manually commanded to STOP.  Must reset to continue.
			IF CommandReset THEN
				MachineState := mINIT;
			END_IF
		
		mEMERGENCY_STOP:													//Safety-related fault has occurred, must clear fault and reset before continuing.
			IF NOT EStop_Condition THEN
				MachineState := mWAIT_RESET;
			END_IF
		
		mWAIT_RESET:														//After safety-related fault has been cleared, press reset button to continue.
			IF CommandReset THEN
				MachineState := mINIT;
			END_IF
	END_CASE;
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

