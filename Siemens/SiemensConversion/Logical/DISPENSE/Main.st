
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	
	//General algorithm used here: Check out VBlock into "VBlock_At_Dispense[]", check back in once you've done the operations/conditionals involved
	//Ideally this would be made as 2x FBK instances w/ VAR_INTERNAL, but for cross-compatibility's sake I'm keeping to basic flow control only.
	FOR ii := 0 TO MAX_IDX_DISPENSE_VALVES BY 1 DO    							//Iterate once through for each dispense valve
		VBlock_At_Dispense[ii] := VBlocks[VBlock_Idx_At_Dispense[ii]]; 			//"Check Out" the current VBlock at the dispense unit.  VBlock_Idx_At_Dispense[] set in MOTOR task.
		
		//On cycle where fractional position transitions across the dispense fractional offset (both 0-100%), if there is a part present and it hasn't already been dispensed on by this valve, flag to dispense now
		IF (VBlockFractPosAtPiP_pct > recDisp_Fractional_Offset_Pct[ii] 		//Current fractional position greater than programed dispense offset percentage
			AND _VBlockFractPosAtPiP_pct < recDisp_Fractional_Offset_Pct[ii] 	//Last scan's fractional position less than programed dispense offset percentage
			AND VBlock_At_Dispense[ii].PartPresent 								//VBlock currently at this dispense valve has a part present
			AND NOT VBlock_At_Dispense[ii].Dispensed[ii])						//Vblock currently at this valve has not been dispensed on by this valve yet, in case the machine moved backwards then forwards and this would be triggered 2x
			OR Command_Manual_Dispense_Trigger[ii] THEN							//Also fire on manual command, ignoring the rest of the conditions		
			
			(*osFlagDispenseNow[ii] := TRUE;*)									//one-shot flag to command dispense cycle to start
			oTriggerDispense[ii] := TRUE;
			VBlock_At_Dispense[ii].Dispensed[ii] := TRUE; 						//Mark checked-out vblock that it has already been dispensed on by this valve (thus the 2 iterators).
		END_IF
		
		VBlocks[VBlock_Idx_At_Dispense[ii]] := VBlock_At_Dispense[ii];			//"Check In" VBlock at dispense unit
		_VBlock_At_Dispense[ii] := VBlock_At_Dispense[ii];						//Latch current cycle's values to use next cycle;
		_VBlock_Idx_At_Dispense[ii] := VBlock_Idx_At_Dispense[ii];				//Latch current index of vblock at dispense to use next cycle;
		Command_Manual_Dispense_Trigger[ii] := FALSE;							//RESET manual dispense trigger bit
	END_FOR
	
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

