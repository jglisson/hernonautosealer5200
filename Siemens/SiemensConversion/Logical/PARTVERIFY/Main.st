
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
	//
	IF VBlockIdxAtPiP <> _VBlockIdxAtPiP THEN									//Trigger on the scan after the VBlock Idx at the PiP sensor changes
		brsmemset(ADR(VBlocks[VBlockIdxAtPiP]), 0, SIZEOF(VBlock_typ));			//Overwrite all memory that that structure occupies with 0 to wipe out information from the last time it was here
		VBlocks[VBlockIdxAtPiP].PartPresent := iPartPresentSensor;				//Latch the indicator in the VBlock structure with the value of the Part Present sensor.
	END_IF
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

