﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.3.3.196?>
<SwConfiguration CpuAddress="SL1" xmlns="http://br-automation.co.at/AS/SwConfiguration">
  <TaskClass Name="Cyclic#1">
    <Task Name="MAIN" Source="MAIN.prg" Memory="UserROM" Description="Master state machine." Language="IEC" Debugging="true" />
    <Task Name="LIGHTS" Source="LIGHTS.prg" Memory="UserROM" Description="Set stack lights according to &quot;MachineState&quot;" Language="IEC" Debugging="true" />
    <Task Name="LOAD" Source="LOAD.prg" Memory="UserROM" Description="Handle LOAD station (placeholder at the moment)" Language="IEC" Debugging="true" />
    <Task Name="PARTVERIFY" Source="PARTVERIFY.prg" Memory="UserROM" Description="Handle the part verify sensor" Language="IEC" Debugging="true" />
    <Task Name="DISPENSE" Source="DISPENSE.prg" Memory="UserROM" Description="Handle Dispense triggering and IO" Language="IEC" Debugging="true" />
    <Task Name="CURE" Source="CURE.prg" Memory="UserROM" Description="Handle DO's for curing lights" Language="IEC" Debugging="true" />
    <Task Name="INSPECT" Source="INSPECT.prg" Memory="UserROM" Description="Handle inspection camera IO, triggering, status, etc." Language="IEC" Debugging="true" />
    <Task Name="ACCEPT" Source="ACCEPT.prg" Memory="UserROM" Description="Handle the ACCEPT kicker" Language="IEC" Debugging="true" />
    <Task Name="REJECT" Source="REJECT.prg" Memory="UserROM" Description="Handle the REJECT kicker" Language="IEC" Debugging="true" />
    <Task Name="VERIFY" Source="VERIFY.prg" Memory="UserROM" Description="Handle the EJECT VERIFY sensor" Language="IEC" Debugging="true" />
    <Task Name="MOTOR" Source="MOTOR.prg" Memory="UserROM" Description="Handle the motor operation, calculate VBlock idx's at stations" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#2" />
  <TaskClass Name="Cyclic#3" />
  <TaskClass Name="Cyclic#4">
    <Task Name="DEFAULTPAR" Source="DEFAULTPARAMS.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#5" />
  <TaskClass Name="Cyclic#6" />
  <TaskClass Name="Cyclic#7" />
  <TaskClass Name="Cyclic#8" />
  <Binaries>
    <BinaryObject Name="udbdef" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="TCData" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="arconfig" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="User" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Role" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="iomap" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="asfw" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="ashwac" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="ashwd" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="sysconf" Source="" Memory="SystemROM" Language="Binary" />
  </Binaries>
  <Libraries>
    <LibraryObject Name="AsBrMath" Source="Libraries.AsBrMath.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsBrStr" Source="Libraries.AsBrStr.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsBrWStr" Source="Libraries.AsBrWStr.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="asstring" Source="Libraries.asstring.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="brsystem" Source="Libraries.brsystem.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="runtime" Source="Libraries.runtime.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Convert" Source="Libraries.Convert.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="standard" Source="Libraries.standard.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="sys_lib" Source="Libraries.sys_lib.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MTBasics" Source="Libraries.MTBasics.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MTTypes" Source="Libraries.MTTypes.lby" Memory="UserROM" Language="Binary" Debugging="true" />
  </Libraries>
</SwConfiguration>